#include "Board.h"
#include <iostream>

using namespace std;

Board::Board()
{
	snake=Snake();
	isAppleOnBoard = false;
	for (int i=0;i<10;i++)
	{
		for (int j=0;j<10;j++)
		{
			board[i][j] = 0;
			for (Segment element : snake.snake)
			{
				if (element.getX()==i&&element.getY()==j)
				{
					board[i][j] = 1;
				}
			}
		}
	}
}

void Board::createApple()
{
		int a = rand() % 10;
		int b = rand() % 10;
		board[a][b] = 2;
}


bool Board::isThereAnApple(int x, int y)
{
	return (board[x][y] == 2);
}

void Board::Update()
{
	if (isThereAnApple(snake.snake.back().getX(), snake.snake.back().getY())) {

		board[snake.snake.back().getX()][snake.snake.back().getY()] = 1;
		Board::createApple();
	}
		
	else
	{
		board[snake.snake.front().getX()][snake.snake.front().getY()] = 0;
		board[snake.snake.back().getX()][snake.snake.back().getY()] = 1;
		snake.delFirstSeg();
	}
}

//bool Board::checkCollision(int a, int b,Snake& snake)
//{
//	bool collision = false;
//	for (Segment element : snake.snake)
//	{
//		if (a == element.getX() && b == element.getY()) {
//			collision = true;
//			cout << endl << "Mamy kolizje" << endl;
//		}
//	}
//	return collision;
//} 

int Board::getCellHeight()
{
	return cellHight;
}

int Board::getCellWidth()
{
	return cellWidth;
}




