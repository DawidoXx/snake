#pragma once
#include "Snake.h"

class Board
{
private:
	const int cellHight = 10;
	const int cellWidth = 10;
	bool isAppleOnBoard;

public:

	Snake snake;
	int board[10][10];

	int getCellHeight();
	int getCellWidth();
	bool isThereAnApple(int x, int y);
	Board();
	void createApple();
	void Update();
	//bool checkCollision(int a, int b,Snake& snake);
};

