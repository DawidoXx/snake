﻿ // Snake.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include <SFML/Graphics.hpp>
#include "Board.h"

int main()
{
	sf::RenderWindow window(sf::VideoMode(1000, 1000), "SFML works!");
	sf::RectangleShape apple(sf::Vector2f(100.0f,100.0f));
	sf::RectangleShape cos(sf::Vector2f(100.0f, 100.0f));
	cos.setFillColor(sf::Color::Blue);

	sf::Texture appleText;
	//plik w folderze z main.cpp
	appleText.loadFromFile("apple.jpg");
	apple.setTexture(&appleText);

	
	Board board;
	board.createApple();
	bool gameOver = false;
	string lastDir = "down";
	int d = 0;

	while (window.isOpen()&&!gameOver)
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			//event.key.code = lastDir;
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			case sf::Event::Resized:
				//std::cout << event.size.height << event.size.width << std::endl;
				//%i int %f double %v bool
				printf("New window hight: %i. New window width: %i.\n", event.size.height, event.size.width);
				break;

			case sf::Event::KeyPressed:
				if(event.key.code==sf::Keyboard::Down)
				{
					lastDir = "down";
					gameOver = !board.snake.moveSnake(lastDir);
					if(!gameOver)
						board.Update();
				}
				if (event.key.code == sf::Keyboard::Left)
				{
					lastDir = "left";
					gameOver = !board.snake.moveSnake(lastDir);
					if (!gameOver)
						board.Update();
				}
				if (event.key.code == sf::Keyboard::Up)
				{
					lastDir = "up";
					gameOver = !board.snake.moveSnake(lastDir);
					if (!gameOver)
						board.Update();
				}
				if (event.key.code == sf::Keyboard::Right)
				{
					lastDir = "right";
					gameOver = !board.snake.moveSnake(lastDir);
					if (!gameOver)
						board.Update();
				}
				break;

			//case sf::Event::TextEntered:
			//	if (event.text.unicode<128)
			//	printf("%c", event.text.unicode);
			//	break;
			
			}
				
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				sf::Vector2i mousePos = sf::Mouse::getPosition(window);
				apple.setPosition((float)mousePos.x-50.0f, static_cast<float>(mousePos.y)-50.0f);
			}

		}

		if (gameOver)
			cout << "You lost:(";

		d++;
		if (d > 100) {
			cout << "Witaj przywoływaczu" << endl;
			d %= 100;

			gameOver = !board.snake.moveSnake(lastDir);
			if (!gameOver)
				board.Update();
		}

		//rysowanie planszy
		window.clear();
		
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (board.board[i][j] == 1) {
					cos.setPosition(sf::Vector2f(100.0f * i, 100.0f * j));
					window.draw(cos);
				}
				if (board.board[i][j] == 2)
					apple.setPosition(sf::Vector2f(100.0f * i, 100.0f * j));
				window.draw(apple);
			}
		}
		window.display();
	}
}




