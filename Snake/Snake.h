#pragma once
#include <vector>
#include <string>
#include "Board.h"
using namespace std;

class Segment
{

	int x;
	int y;
	string colour;

public:
	Segment(int a, int b, string col);
	int getX()
	{
		return x;
	}
	int getY()
	{
		return y;
	}
};

class Snake
{
private:
	
	//string lastDirection=;

public:
	vector<Segment> snake;

	Snake();
	vector<Segment> getVecSnake();
	void delFirstSeg();

	void addNewSeg(int a, int b);
	
	void setSnake(Segment segment);
	bool checkCollision(int a, int b);

	bool moveSnake(string whereAreWeGoing);
};

