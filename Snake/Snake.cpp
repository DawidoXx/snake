#include "Snake.h"
#include <iostream>


Snake::Snake()
{
	Segment segment1(0, 0, "blue");
	Segment segment2(0, 1, "blue");
	Segment segment3(0, 2, "blue");
	snake.push_back(segment1);
	snake.push_back(segment2);
	snake.push_back(segment3);

}

void Snake::delFirstSeg()
{
	snake.erase(snake.begin());
}

void Snake::addNewSeg(int a, int b)
{
	Segment seg(a, b, "blue");
	snake.push_back(seg);
}

vector<Segment> Snake::getVecSnake()
{
	return snake;
}

void Snake::setSnake(Segment segment)
{
	snake.push_back(segment);
}

bool Snake::checkCollision(int a, int b)
{
	bool collision = false;
	for (Segment element : snake)
	{
		if (a == element.getX() && b == element.getY()) {
			collision = true;
			cout << endl << "Mamy kolizje" << endl;
		}
	}
	return collision;
}

bool Snake::moveSnake(string whereAreWeGoing)
{
	int a = snake.back().getX();
	int b = snake.back().getY();

	if(whereAreWeGoing=="up")
	{
		b -= 1;
		if (b < 0)
			b += 10;
	}
	if(whereAreWeGoing=="down")
	{
		b += 1;
		if (b > 9)
			b %= 10;
	}
	if(whereAreWeGoing=="right")
	{
		a += 1;
		if (a > 9)
			a %= 10;
	}
	if(whereAreWeGoing=="left")
	{
		a -= 1;
		if (a < 0)
			a += 10;
	}

	if(checkCollision(a, b))
	{
		return false;
	}

	Segment segment(a, b, "red");
	snake.push_back(segment);
	return true;
}

Segment::Segment(int a,int b,string col)
{
	x = a;
	y = b;
	colour = col;
}
